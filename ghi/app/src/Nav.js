import { BrowserRouter, NavLink } from "react-router-dom";

function Nav() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          {/* <NavLink className="navbar-brand">Conference GO!</NavLink> */}
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link" aria-current="page" to="/">
                Home
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/locations/new">
                New location
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/conferences/new">
                New conference
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/attendees/new">
                New attendee
              </NavLink>
              <NavLink className="nav-link" aria-current="page" to="/attendees">
                Attendee list
              </NavLink>

              <li className="nav-item">
                <a className="nav-link" aria-current="page" href="/presentations/new">New presentation</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }

  export default Nav;
