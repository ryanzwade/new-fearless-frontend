import Nav from './Nav';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import LocationForm from './LocationForm';
// import LocationList from './LocationList'
import ConferenceForm from './ConferenceForm';
import AttendForm from './AttendForm';
import AttendeesList from './AttendeesList';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            {/* <Route index element={<LocationList />} /> */}
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            {/* <Route index element={<ConferenceList />} /> */}
            <Route path="new" element={<ConferenceForm />} />
          </Route>

          <Route path="/attendees" element={<AttendeesList />} />
          <Route path="/attendees/new" element={<AttendForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
