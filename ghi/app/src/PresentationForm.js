import React, {useEffect, useState} from 'react';

function PresentationForm(props) {
  const [conferences, setConferences] = useState([]);
  const [presenterName, setPresenterName] = useState("");
  const [presenterEmail, setPresenterEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.presenter_name = presenterName;
    data.presenter_email = presenterEmail;
    data.company_name = companyName;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference

    console.log(data);

    const presentationUrl = `http://localhost:8000/${conference}presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setPresenterName('');
      setPresenterEmail('');
      setCompanyName('');
      setTitle('');
      setSynopsis('');
      setConference('');

        }
    }

    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
      }

      const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
      }

      const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
      }
      const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }
      const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
      }

      const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
      }

      return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={presenterName} onChange={handlePresenterNameChange} placeholder="Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                            <label htmlFor="presenter_name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={presenterEmail} onChange={handlePresenterEmailChange}placeholder="Presenter Email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                            <label htmlFor="presenter_email">Presenter Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={title} onChange={handleTitleChange}placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                        <textarea onChange={handleSynopsisChange}placeholder="Synopsis" name="synopsis" id="synopsis" className="form-control" rows="3"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                    <input value={companyName} onChange={handleCompanyNameChange} required type="text" name="company_name" id="company_name" className="form-control"/>
                        <label htmlFor="company_name">Company name</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                        <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                    return (
                      <option key={conference.id} value={conference.href}>
                      {conference.name}
                      </option>
                    );
                  })}
                </select>
                </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>

      );
    }

    export default PresentationForm;
